#!/bin/python

import os
import re

from reportlab.pdfgen import canvas
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Indenter
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_JUSTIFY
from reportlab.lib.pagesizes import letter

def code_to_text(regpattern = 'py$',
	spath = os.getcwd(), 
	dpath = os.path.join(os.getcwd(),"code.pdf"),
	fontsz = 10, 
	fontface = "Helvetica", 
	left = 5,
	right = 5,
	top = 5,
	bottom = 5, 
	cmnt = '#', 
	tabsize = 8):
	"""Converts numerous source files into one pdf file to print and present
	
		which fonts can I use FAQ http://www.reportlab.com/software/documentation/faq/#2.6.1
	"""
	story = []
	
	styles = getSampleStyleSheet()
	styles.add(ParagraphStyle(name = 'Custom', fontName = fontface, fontSize = fontsz))
	
	for path, dirs, files in os.walk(spath):
		if '.git' in dirs:
			dirs.remove('.git')
		for f in files:
			previousIndent = 0
			head = cmnt + ' ' + f + ' ' + cmnt
			if re.search(regpattern, f) != None:
				print(f)
				story.append(Paragraph(head, styles["Custom"]))
				with open(os.path.join(path, f)) as fd:
					for line in fd.readlines():
						#kludge here
						indent = len(line) - len(line.lstrip()) #lstrip(what?) -- /ts or ' 's?
						story.append(Indenter(left = (indent - previousIndent) * tabsize))
						previousIndent = indent
						#kludge ends
						story.append(Paragraph(line, styles["Custom"]))
				fd.close()
				
	story.append(Spacer(1, 12))

	doc = SimpleDocTemplate(dpath, 
							leftMargin=left, 
							rightMargin=right, 
							topMargin=top, 
							bottomMargin=bottom)
	
	doc.build(story)
	
			
